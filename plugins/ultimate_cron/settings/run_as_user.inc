<?php
/**
 * @file
 * Run As User settings for Ultimate Cron.
 */

$plugin = array(
  'title' => t('Run As User'),
  'description' => t('Run As User settings.'),

  'handler' => array(
    'file' => 'run_as_user.class.php',
    'class' => 'UltimateCronRunAsUserSettings',
    'parent' => 'UltimateCronSettings',
  ),
);
