<?php
/**
 * @file
 * Run As User settings for Ultimate Cron.
 */

define('ULTIMATE_CRON_RUN_AS_ANONYMOUS', 1);
define('ULTIMATE_CRON_RUN_AS_USER', 2);

/**
 * Run As User settings plugin class.
 */
class UltimateCronRunAsUserSettings extends UltimateCronSettings {
  static private $users = array();

  /**
   * Constructor.
   */
  public function __construct($name, $plugin, $log_type = ULTIMATE_CRON_LOG_TYPE_NORMAL) {
    parent::__construct($name, $plugin, $log_type);
    $this->options['method'] = array(
      ULTIMATE_CRON_RUN_AS_ANONYMOUS => t('Run as anonymous'),
      ULTIMATE_CRON_RUN_AS_USER => t('Run as a specific user'),
    );
  }

  /**
   * Implements hook_cron_pre_invoke().
   */
  public function cron_pre_invoke($job) {
    global $user;
    self::$users[$job->name][] = $user;

    $settings = $job->getSettings('settings');

    // No switching necessary if anonymous.
    if ($settings['run_as_user']['method'] == ULTIMATE_CRON_RUN_AS_ANONYMOUS) {
      if (!$user || empty($user->uid)) {
        return;
      }
      else {
        // We want anonymous, but current user isn't anonymous.
        $switch_user = drupal_anonymous_user();
      }
    }
    elseif ($settings['run_as_user']['method'] != ULTIMATE_CRON_RUN_AS_USER) {
      return;
    }
    elseif (empty($settings['run_as_user']['username'])) {
      // No switching possible of no username.
      return;
    }
    else {
      // Load user and check validity.
      $switch_user = user_load_by_name($settings['run_as_user']['username']);

      if (empty($switch_user)) {
        watchdog('ultimate_cron', 'User @username not found. Running job @jobname as @currentusername.', array(
          '@username' => $settings['run_as_user']['username'],
          '@jobname' => $job->name,
          '@currentusername' => isset($user->name) ? $user->name : t('anonymous'),
        ), WATCHDOG_ERROR);
        return;
      }
    }

    UltimateCronLogger::log('ultimate_cron', 'Switching from @srcname (@srcuid) to @dstname (@dstuid)', array(
      '@srcname' => isset($user->name) ? $user->name : t('anonymous'),
      '@srcuid' => $user->uid,
      '@dstname' => isset($switch_user->name) ? $switch_user->name : t('anonymous'),
      '@dstuid' => $switch_user->uid,
    ), WATCHDOG_INFO);

    $user = $switch_user;
  }

  /**
   * Implements hook_cron_post_invoke().
   */
  public function cron_post_invoke($job) {
    global $user;
    $user = array_pop(self::$users[$job->name]);
  }

  /**
   * Default settings.
   */
  public function defaultSettings() {
    return array(
      'method' => ULTIMATE_CRON_RUN_AS_ANONYMOUS,
      'username' => '',
    );
  }

  /**
   * Label for setting.
   */
  public function settingsLabel($name, $value) {
    switch ($name) {
      case 'method':
        return $this->options[$name][$value];
    }
    return parent::settingsLabel($name, $value);

  }

  /**
   * Settings form.
   */
  public function settingsForm(&$form, &$form_state, $job = NULL) {
    $elements = &$form['settings'][$this->type][$this->name];
    $defaults = &$form_state['default_values']['settings'][$this->type][$this->name];
    $values = &$form_state['values']['settings'][$this->type][$this->name];

    $elements['method'] = array(
      '#type' => 'select',
      '#title' => t('Switch user'),
      '#description' => t('Run as anonymous or a specific user.'),
      '#options' => $this->options['method'],
      '#default_value' => $values['method'],
      '#fallback' => TRUE,
      '#required' => TRUE,
    );

    $elements['username'] = array(
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#description' => t('Choose the user that the job(s) should run as.'),

      // Since we just need simple user lookup, we can use the simplest function
      // of them all, user_autocomplete().
      '#autocomplete_path' => 'user/autocomplete',
      '#default_value' => $values['username'],
      '#fallback' => TRUE,
      '#element_validate' => array('ultimate_cron_run_as_user_element_validate_username'),
    );
    if ($job) {
      $elements['username']['#states'] = array(
        'invisible' => array(
          ':input[name="settings[' . $this->type . '][' . $this->name . '][method]"]' => array(
            array('value' => ULTIMATE_CRON_RUN_AS_ANONYMOUS),
          ),
        ),
        'disabled' => array(
          ':input[name="settings[' . $this->type . '][' . $this->name . '][method]"]' => array(
            array('value' => ULTIMATE_CRON_RUN_AS_ANONYMOUS),
          ),
        ),
      );
      if ($defaults['method'] == ULTIMATE_CRON_RUN_AS_ANONYMOUS) {
        $elements['username']['#states']['invisible'][':input[name="settings[' . $this->type . '][' . $this->name . '][method]"]'][] = array('value' => '');
        $elements['username']['#states']['disabled'][':input[name="settings[' . $this->type . '][' . $this->name . '][method]"]'][] = array('value' => '');
      }
    }
  }
}
